<?php

namespace App\Http\Controllers;

use Pesapal;
use App\Models\User;
use App\Http\Controllers\KopoKopoController as KopokopoLib;
use App\Models\Payment;
use App\Mail\VegOrderSent;
use App\Mail\PaidOrderSent;
use Illuminate\Http\Request;
use App\Mail\RecipeSubmitted;
use App\Models\CookbookPurchase;
use App\Mail\VegetableOrderReceived;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\CookbookPaymentReceived;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\RecipeSubmitRequest;
use App\Http\Controllers\PesapalController as PesapalKE;

class CookbookController extends Controller
{
    function index()
    {
        // dd(app()->getLocale());
        if (!session()->has('locale')) {
            return redirect('locale/en');
        }

        $products = collect(config('cookbook.products'));
        $veges = collect(config('cookbook.veges'));
        

        if (request()->has('filter')) {
            $products = $products->groupBy('type');
            $products = $products[request()->filter];
        }

        //return json_encode($products, true);

        $slides = [
            '/resized IREN/Kunde-transformed.jpeg',
            '/resized IREN/Managu-transformed.jpeg',
            '/resized IREN/Mitoo-transformed.jpeg',
            '/resized IREN/Murenda-transformed.jpeg',
            '/resized IREN/Sagaa-transformed.jpeg',
            '/resized IREN/Tsimboka-transformed.jpeg',
        ];

        return view('cookbook.index', [
            'products' => $products,
            'veges'    => $veges,
            'slides'   => $slides,
        ]);
    }

    function vegesOrder($encryptedKey)
    {
        $key = decrypt($encryptedKey);
        $product = config('cookbook.veges')[$key];

       return view('cookbook.view-veges', [
            'product'  => $product,
            'key'      => $key,
            'purchase' => true,
        ]);
    }

    function addVegiesOrderToCart($id)
    {
        $order = request()->all();
        $item = config('cookbook.veges')[$id];
        // dd($item);

        //confirm total price is correct
        $quantity = $order['quantity'];
        $vege = explode('-',$order['veges_select']);

        $serving = $vege[0];
        $price = $vege[1];

        $total = $quantity * $price;
        // dd($order, $item, $total);

        \ShoppingCart::add($id, $item['name'].' / '.$item['engName'], $order['quantity'], $price, ['total' => $total, 'serving' => $serving, 'type' => 'vegetable', 'picture' => $item['product_bag']]);

        return redirect('cookbook#shop');
    }

    function display($encryptedKey)
    {
        $key = decrypt($encryptedKey);
        $product = config('cookbook.products')[$key];

        if (request()->has('isCard')) {
            $iframe = $this->startPayment($product, $key);
        }

        // dd($iframe);

        return view('cookbook.view-product', [
            'product'  => $product,
            'key'      => $key,
            'purchase' => true,
            'iframe'   => @$iframe
        ]);
    }

    function purchase($encryptedKey)
    {
        $key = decrypt($encryptedKey);

        $product = config('cookbook.products')[$key];

        # Check if book is purchased
        $purchase = CookbookPurchase::where('user_id', auth()->id())->where('product_key', $key);
        if ($purchase->count()) {
            return redirect('cookbook/my-purchases');
        }

        return view('cookbook.purchase', [
            'product' => $product,
            'key'     => $key
        ]);
    }

    function activatePurchase($key, $token)
    {
        $key = decrypt($key);
        $paymentID = decrypt($token);

        $product = config('cookbook.products')[$key];
        $payment = Payment::find($paymentID);

        if ($payment->amount != $product['price']) {
            abort(403, 'There is a problem with your transaction. Please contact customer support for help');
        }

        # Send receipt
        Mail::to(auth()->user())->send(new CookbookPaymentReceived($payment));

        # record purchase
        CookbookPurchase::create([
            'user_id'     => auth()->id(),
            'product_key' => $key,
            'payment_id'  => $paymentID
        ]);

        $payment->update(['user_id' => auth()->user()->id]);

        return redirect('/cookbook/my-purchases');
    }

    function myPurchases()
    {
        return view('cookbook.my-purchases', [
            'purchases' => CookbookPurchase::with('payment')->orderBy('created_at', 'desc')->where('user_id', auth()->id())->get()
        ]);
    }

    public function showPurchase($id)
    {
        $item = CookbookPurchase::with('payment')->find($id); // Adjust the model and variable accordingly

        if ($item) {
            // Return a partial view with the item data (this is a Blade view)
            return view('cookbook.modals.purchase-partial', compact('item'));
        }

        return response()->json(['error' => 'Item not found'], 404);
    }


    function download($purchaseID, $itemID)
    {
        // dd($purchaseID, $itemID);
        $purchase = CookbookPurchase::find($purchaseID);

        if (auth()->id() != $purchase->user_id) {
            abort(403, 'You do not have the privileges to access this resource');
        }

        $product = config('cookbook.products')[$itemID];
        $file = storage_path() . '/app/' . $product['file'];

        $filetype = filetype($file);
        $filename = basename($file);
        header("Content-Type: " . $filetype);
        header("Content-Length: " . filesize($file));
        header("Content-Disposition: attachment; filename=" . $filename);
        readfile($file);
    }

    function showSales()
    {
        $sales = CookbookPurchase::orderBy('created_at', 'desc')->paginate();
        return view('admin.cookbook.sales', [
            'sales' => $sales
        ]);
    }

    public function startPayment($product, $key)
    {
        $ref = Pesapal::random_reference();
        $row = [
            'processor'             => 'PESAPAL',
            'transaction_reference' => $ref,
            'transaction_timestamp' => now(),
            'amount'                => $product['price'][app()->getLocale()],
            'user_id'               => auth()->id(),
            'pesapal_status'        => 'NEW',
            'product_key'           => $key
        ];
        //        dd($product);

        $payment = Payment::create($row);

        $details = array(
            'amount'      => $payment->amount,
            'description' => $product['name'][app()->getLocale()],
            'type'        => 'MERCHANT',
            'first_name'  => explode(' ', auth()->user()->name)[0],
            'last_name'   => explode(' ', auth()->user()->name)[1],
            'email'       => auth()->user()->email,
            'phonenumber' => auth()->user()->telephone,
            'reference'   => $ref,
            'height'      => '1000px',
            'currency'    => 'KES'
        );
        $iframe = Pesapal::makePayment($details);

        return $iframe;
    }

    function showRecipeSubmitForm()
    {
        return view('cookbook.submit');
    }

    function submitRecipe(RecipeSubmitRequest $request)
    {
        Mail::to(['growthpad@irenkenya.com', 'nelson@sodium.co.ke'])->send(new RecipeSubmitted($request->all()));
        session()->flash('successbox', ['Your request has been received. Someone will be in touch soon!']);
        return redirect('submit/recipe?view=success');
    }

    function addToCart($id)
    {
        $item = config('cookbook.products')[$id];
        // dd($item);
        \ShoppingCart::add($id, $item['name'][app()->getLocale('en')], 1, $item['price'][app()->getLocale('en')], ['type' => 'recipe', 'picture' => $item['picture']]);

        return redirect('cookbook#shop');
    }


    function displayCart()
    {
        $orderItems = collect(\ShoppingCart::all());

        return view('cookbook.cart', [
            'items'  => collect($orderItems),
        ]);
    }

    public function checkout(Request $request)
    {
        if (!isset(auth()->user()->pick_up_address) || !isset(auth()->user()->telephone)) {
            return redirect('/profile/edit?from=checkout')->with('success', 'Please set up a default address and telephone for your orders');
        }

        $orderItems = collect(\ShoppingCart::all());
        // dd($orderItems);
        $orderDescription = null;
        $serializedOrder = null;

        if ($orderItems->count()) {

            if ($request->till) {

                $payment = Payment::find($request->p);
                $grandTotal = $payment->amount;
                $merchantRef = $payment->transaction_reference;

            }else{

                $orderItems->each(function ($item) use (&$orderDescription, &$serializedOrder) {

                    $orderDescription[] = $item->name; 
                    $serializedOrder[] = ['id' => $item->id ,'name' => $item->name, 'price' => $item->price, 'quantity' => $item->qty, 'type' => $item->type];

                });
                $description = implode(' ', $orderDescription);
                $serializedOrder = json_encode($serializedOrder);
                
                $grandTotal = 0;
                foreach ($orderItems as $item) {
                    $grandTotal = $grandTotal + $item->total;
                }

                $merchantRef = PesapalKE::generateMerchantReference();
                $name = Auth::user()->name;
                $nameArr = explode(' ', $name); // extract name to single components

                $callbackUrl = env('PESAPAL_CALLBACK_ROUTE', 'handlePesapalCallback');

                $orderPayload = array(
                    'phone_number' => Auth::user()->telephone,
                    // 'phone_number' => '0757573875',
                    'amount' => $grandTotal,
                    // 'amount' => 1.00,
                    'merchantRef' => $merchantRef,
                    'description' => 'Payment for items: '.$description,
                    'callbackurl' => route($callbackUrl),
                    // 'callbackurl' => 'https://18c0-197-232-61-196.ngrok-free.app/pay/pesapal/confirmation',
                    'redirect_mode' => '',
                    'branch' => "IREN",
                    'first_name' => $nameArr[0] ?? "",
                    'middle_name' => $nameArr[1] ?? "",
                    'last_name' => $nameArr[2] ?? "",
                    'email_address' => Auth::user()->email
                );

                $payment = Payment::create([
                    'processor'             => isset($request->till)? 'M-PESA_TILL' :'PESAPAL',
                    'transaction_reference' => $merchantRef,
                    'transaction_timestamp' => now(),
                    'amount'                => $grandTotal,
                    'user_id'               => auth()->id(),
                    'pesapal_status'        => 'NEW',
                    'serialized_products'   => $serializedOrder,
                    'product_type' => 'Serialized product',
                ]);
            }
            // $details = array(
            //     'amount'      => $total,
            //     'description' => $products,
            //     'type'        => 'MERCHANT',
            //     'first_name'  => explode(' ', auth()->user()->name)[0],
            //     'last_name'   => explode(' ', auth()->user()->name)[1],
            //     'email'       => auth()->user()->email,
            //     'phonenumber' => auth()->user()->telephone,
            //     'reference'   => $ref,
            //     'height'      => '1000px',
            //     'currency'    => 'KES'
            // );
            // $iframe = Pesapal::makePayment($details);

            if (!isset($request->till)) {
                $paymentUrl = PesapalKE::submitOrderRequest($orderPayload);
            }
            
            // isset($request->till) ? session()->put('gTotal', $grandTotal): false;

            return view('cookbook.checkout', [
                'items'  => collect($orderItems),
                'paymentUrl' => $paymentUrl ?? '#',
                'paymentId' => $payment->id,
                'transReference' => $merchantRef ?? '',
                'grandTotal' => $grandTotal ?? 0
            ]);

        }else
        {
            abort(403, 'Please choose some cart items before checkout');
        }

    }

    public function setCustomAddress()
    {
        $request = request()->all();

        $transaction = Payment::where('transaction_reference', $request['transReference']);
        $transaction->update(['pick_up_station' => $request['custom_address']]);

        return response()->json(['success' => true, 'address' => $request['custom_address']]);
    }

    public function handlePesapalCallback()
    {
        $response = PesapalKE::handleResponseCallback();
        $response = json_decode($response);

        $status = isset($response->payment_status_description) ? $response->payment_status_description : 'Failed';

        if ($status != 'Completed')
        {
            // Handle failed payments
            return redirect('cookbook/cart/checkout')->with('errorbox', 'Payment failed, please try again.');

        }elseif ($status == 'Completed') {
            // Handle successful payments
            $message = $response->message;
            $merchantRef = request()->get('OrderMerchantReference');
            $payment = Payment::where('transaction_reference', $merchantRef)->first();
            // Send payment confirmation notification
            Mail::to(auth()->user())->send(new CookbookPaymentReceived($payment, false));

            $shippingAddress = $payment->pick_up_station ?? auth()->user()->pick_up_address;

            // Send order details to client (iren)
            $order = [
                'names' => auth()->user()->name,
                'email' => auth()->user()->email,
                'telephone' => auth()->user()->telephone,
                'location' => $shippingAddress,
                'tnxRef' => $merchantRef,
                'total' => $payment->amount
            ];
            Mail::to(config('settings.team'))->send(new PaidOrderSent($order, $payment->serialized_products));
            // Mail::to('mosesgitau380@gmail.com')->send(new PaidOrderSent($order, $payment->serialized_products));
            $payment->update([
                'processor' => $response->payment_method,
                'pesapal_tracking_id' => $response->order_tracking_id,
                'pesapal_status' => 'COMPLETED',
            ]);

            CookbookPurchase::create([
                'user_id'     => auth()->id(),
                'product_key' => 0,
                'payment_id'  => $payment->id
            ]);

            # Empty cart
            \ShoppingCart::destroy();

            return view('cookbook.payment-success', ['payment' => $payment]);
        }
    }

    function initiateStkPushPayment(Payment $payment, Request $request)
    {
        $telephone = Auth::user()->telephone;
        if (substr($telephone, 0, 2) == '07') {
            $telephone = '+254' . substr($telephone, 1);
        }

        $name = Auth::user()->name;
        $nameArr = explode(' ', $name); // extract name to single components

        $payload = [
            'firstName' => $nameArr[0] ?? '',
            'lastName' => $nameArr[1] ?? '',
            // 'phoneNumber' => '+254757573875',
            'phoneNumber' => $telephone,
            'amount' => $payment->amount,
            'email' => Auth::user()->email ?? '',
            'callbackUrl' => route('handleKopokopoCallback'),
            // 'callbackUrl' => " https://47e3-197-232-61-209.ngrok-free.app/api/checkout/mpesa",
            'paymentID' => $payment->id,
            'customerID' => Auth::id(),
            'pick_up' => $payment->pick_up_station ?? Auth::user()->pick_up_address
        ];
        // dd($payload);
        $response = KopokopoLib::initiateStkPush($payload);
        if ($response['status'] == 'error') {

            session()->flash('errorbox', $response['data']['errorMessage']);
            return redirect()->back();

        }else if ($response['status'] == 'success'){

            $location =  $response['location'];
            // $res_arr = explode('/', $response);
            // $trans_ref = end($res_arr);
            $payment->update(['transaction_reference' => $location]);
    
            return redirect("/cookbook/cart/checkout?till=true&p={$payment->id}&status=pending");
        }
    }

    function checkPaymentStatus(Payment $payment)
    {
        $response = Kopokopolib::getPaymentStatus($payment->transaction_reference);

        if ($response['status'] == 'success') {

            if ($response['data']['status'] == 'Success') {
                return response()->json([
                    'status' => 'complete',
                    'ref' => $response['data']['reference'],
                ]);
            }
            
            if ($response['data']['status'] == 'Failed') {
                return response()->json([
                    'status' => 'failed',
                    'error' => $response['data']['errors']
                ]);
            }

            if ($response['data']['status'] == 'Pending') {
                return response()->json([
                    'status' => 'pending',
                ]);
            }

        }else {

            return response()->json([
                'status' => 'error',
            ]);            
        }
    }

    // function handleKopokopoCallback()
    // {
        // $raw = file_get_contents('php://input');
        // // $raw = request()->all();
        // Storage::disk('local')->append('mpesa.txt', $raw);
        // Storage::disk('local')->put('mpesa.json', $raw);

        // if ($raw['status'] == 'success') {
        //     $data = collect($raw['data']);
        //     $paymentID = $data->metadata->paymentID;
        //     $customerID = $data->metadata->customerID;
        //     $shippingAddress = $data->metadata->pick_up;

        //     $payment = Payment::find($paymentID);
        //     $customer = User::find($customerID);

        //     if ($data->status == 'success') {
        //         # Send email to buyer
        //         Mail::to($customer)->send(new CookbookPaymentReceived($payment, false));

        //         // Send order details to client (iren)
        //         $order = [
        //             'names' => $customer->name,
        //             'email' => $customer->email,
        //             'telephone' => $customer->telephone,
        //             'location' => $shippingAddress,
        //             'tnxRef' => $payment->transaction_reference,
        //             'total' => $payment->amount
        //         ];
        //         Mail::to(config('settings.team'))->send(new PaidOrderSent($order, $payment->serialized_products));

        //         # Record purchase
        //         CookbookPurchase::create([
        //             'user_id'     => auth()->id(),
        //             'product_type' => $payment->product_type,
        //             'product_key' => 0,
        //             'payment_id'  => $payment->id
        //         ]);

        //         $payment->update([
        //             'user_id' => $customerID,
        //             'processor' => 'M-PESA_TILL',
        //             'sender_phone' => $data->senderPhoneNumber,
        //             'pesapal_status' => 'COMPLETED',
        //             'pesapal_tracking_id' => $raw['data']['id'],
        //         ]);

        //     }elseif ($data->status == 'Failed') {
        //         $payment->update([
        //             'pesapal_status' => 'FAILED',
        //             'pesapal_tracking_id' => $raw['data']['id'],
        //         ]);
        //     }
        // }
        // $status = false;
        // Storage::disk('local')->put('mpesa.txt', $raw);

        // if (Storage::disk('local')->exists('mpesa.txt')) {
        //     $status = true;
        // }
        // return response()->json([
        //     'status' => $status,
        //     'raw' => $raw
        // ]);
    // }

    function removeFromCart($raw)
    {
        if ($raw == 'all') {
            \ShoppingCart::destroy();
        } else {
            \ShoppingCart::remove($raw);
        }
        return redirect()->back();
    }

    function paypalSuccess()
    {
        $txn = json_decode(request()->payload);

        //        dd($txn);

        # Record payment
        $payment = Payment::create([
            'processor'             => 'PAYPAL',
            'transaction_reference' => $txn->id,
            'transaction_timestamp' => $txn->create_time,
            'first_name'            => $txn->payer->name->given_name,
            'last_name'             => $txn->payer->name->surname,
            'amount'                => request()->total,
            'user_id'               => auth()->id(),
        ]);


        # Send email to buyer
        Mail::to(auth()->user())->send(new CookbookPaymentReceived($payment, true));

        # Record purchase
        
        collect(\ShoppingCart::all())->where('type', '=', 'recipe')->each(function ($item) use ($payment) {
            CookbookPurchase::create([
                'user_id'     => auth()->id(),
                'product_key' => $item->id,
                'payment_id'  => $payment->id
            ]);
        });

        # Empty cart
        \ShoppingCart::destroy();

        return view('cookbook.payment-success');
    }

    function switchLocale($locale)
    {
        session()->put('locale', $locale);
        return redirect()->back();
    }

    function mpesaTillSuccess(Payment $payment, $reference)
    {
        if (!$payment) {
            abort(403, 'This payment is not valid. Access denied!');
        }

        $payment->update([
            'processor' => 'M-PESA Till',
            'transaction_reference' => $reference,
            'pesapal_status' => 'COMPLETED',
            'user_id' => auth()->id()
        ]);
        
        # Send email to buyer
        Mail::to(auth()->user())->send(new CookbookPaymentReceived($payment, false));

        $shippingAddress = $payment->pick_up_station ?? auth()->user()->pick_up_address;
         // Send order details to client (iren)
         $order = [
            'names' => auth()->user()->name,
            'email' => auth()->user()->email,
            'telephone' => auth()->user()->telephone,
            'location' => $shippingAddress,
            'tnxRef' => $payment->transaction_reference,
            'total' => $payment->amount
        ];
        Mail::to(config('settings.team'))->send(new PaidOrderSent($order, $payment->serialized_products));

        # Record purchase
        CookbookPurchase::create([
            'user_id'     => auth()->id(),
            'product_type' => $payment->product_type,
            'product_key' => 0,
            'payment_id'  => $payment->id
        ]);

        # Empty cart
        \ShoppingCart::destroy();

        return view('cookbook.payment-success', ['payment' => $payment]);
    }

    function orderVeges()
    {
        $order = request()->all();

        Mail::to(config('settings.team'))->send(new VegetableOrderReceived($order));

        session()->flash('successbox', ['Your order has been received. Someone will be in touch soon!']);

        //send notification to customer for placing order
        Mail::to($order['email'])->send(new VegOrderSent($order['names']));

        // empty the vegetables cart
        $items = json_decode($order['order-items'], true);
            foreach ($items as $item) {
                
                \ShoppingCart::remove($item['__raw_id']);
            }

        return redirect('/cookbook#shop');
    }
}
