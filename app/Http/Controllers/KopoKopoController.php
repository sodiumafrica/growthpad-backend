<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kopokopo\SDK\K2;

class KopoKopoController extends Controller
{
    // protected $baseUrl, $clientId, $clientSecret, $apiKey, $k2Service = null;

    // public function __construct()
    // {
    //     $this->baseUrl = config('settings.kopokopo.live') ? "https://api.kopokopo.com/"
    //                                                       : "https://sandbox.kopokopo.com/";

    //     $this->clientId = config('settings.kopokopo.live') ? config('settings.kopokopo.clientId') : "h8J_cL27u7k_mroOmgZX4-Gz6NqKFAcAAyfux-tVI6A";
    //     $this->clientSecret = config('settings.kopokopo.live') ? config('settings.kopokopo.clientSecret') : "dDWCVSueCmJ4W-lurJe-wwkA3rJyB1-vz9s5Io9m03E";
    //     $this->apiKey = config('settings.kopokopo.live') ? config('settings.kopokopo.apiKey') : "UrCDniqVGMaHxFeSMa-98wmo7OEnYhq1g80FW02anWY";

    //     $options = [
    //         'clientId' => $this->clientId, 
    //         'clientSecret' => $this->clientSecret,
    //         'apiKey' => $this->apiKey,
    //         'baseUrl' => $this->baseUrl
    //     ];

    //     $this->k2Service = new K2($options);
    // }

    protected static function getAccessToken($k2Service)
    {
        
        $TokenService = $k2Service->TokenService();

          // check if exists valid token
        if (request()->session()->has('accessToken'))
        {
            $accessToken = request()->session()->get('accessToken');
            $result = $TokenService->introspectToken(['accessToken' => $accessToken]);  
            // dd($result);
            if($result['status'] != 'error' && $result['data']['active'] == true){
              return $accessToken;
            }
        }
          
          // Use the service
        $result = $TokenService->getToken();
        // dd($result);
        if($result['status'] == 'success'){
            $data = $result['data'];
            request()->session()->put('accessToken', $data['accessToken']);
            return $data['accessToken'];
            // echo "It expires in: ".$data['expiresIn'];
        }else{
            abort(500, 'Opps, something went wrong, Please contact support at growthpad@irenkenya.com.');
        }
    }

    public static function initiateStkPush($data)
    {
        // $baseUrl = env('KOPOKOPO_LIVE') ? "https://api.kopokopo.com/"
        //                                 : "https://sandbox.kopokopo.com/";

        // $clientId = env('KOPOKOPO_LIVE') ? env('KOPOKOPO_CLIENT_ID') : "h8J_cL27u7k_mroOmgZX4-Gz6NqKFAcAAyfux-tVI6A";
        // $clientSecret = env('KOPOKOPO_LIVE') ? env('KOPOKOPO_CLIENT_SECRET') : "dDWCVSueCmJ4W-lurJe-wwkA3rJyB1-vz9s5Io9m03E";
        // $apiKey = env('KOPOKOPO_LIVE') ? env('KOPOKOPO_API_KEY') : "UrCDniqVGMaHxFeSMa-98wmo7OEnYhq1g80FW02anWY";

        $baseUrl = "https://api.kopokopo.com/";
        $clientId = "QNZa-x2PuikkitG9oHcD6_jSdZYL3pKrZlHpT7XyPC4";
        $clientSecret = "MUISquKTaNdkQ0kVJU-0JYNcvZYzBDj6AxEjj8_D3t4";
        $apiKey = "f72d526160b1ec5fbde1bfe0a48b7fb22c22576f";

        $options = [
            'clientId' => $clientId, 
            'clientSecret' => $clientSecret,
            'apiKey' => $apiKey,
            'baseUrl' => $baseUrl
        ];

        $k2Service = new K2($options);
        $accessToken = self::getAccessToken($k2Service);
        $StkPushService = $k2Service->StkService();

        $result = $StkPushService->initiateIncomingPayment([
            'paymentChannel' => 'M-PESA STK Push',
            'tillNumber' => 'K851681',
            'firstName' => $data['firstName'],
            'lastName' => $data['lastName'],
            'phoneNumber' => $data['phoneNumber'],
            'amount' => $data['amount'],
            'currency' => 'KES',
            'email' => $data['email'],
            'callbackUrl' => $data['callbackUrl'],
            'metadata' => [
                        'paymentID' => $data['paymentID'],
                        'customerID' => $data['customerID'],
                        'pick_up' => $data['pick_up'],
                    ],
            'accessToken' => $accessToken,
        ]);
   
        return $result;
    }

    public static function getPaymentStatus($location)
    {
        // $baseUrl = env('KOPOKOPO_LIVE') ? "https://api.kopokopo.com/"
        //                                 : "https://sandbox.kopokopo.com/";

        // $clientId = env('KOPOKOPO_LIVE') ? env('KOPOKOPO_CLIENT_ID') : "h8J_cL27u7k_mroOmgZX4-Gz6NqKFAcAAyfux-tVI6A";
        // $clientSecret = env('KOPOKOPO_LIVE') ? env('KOPOKOPO_CLIENT_SECRET') : "dDWCVSueCmJ4W-lurJe-wwkA3rJyB1-vz9s5Io9m03E";
        // $apiKey = env('KOPOKOPO_LIVE') ? env('KOPOKOPO_API_KEY') : "UrCDniqVGMaHxFeSMa-98wmo7OEnYhq1g80FW02anWY";

        $baseUrl = "https://api.kopokopo.com/";
        $clientId = "QNZa-x2PuikkitG9oHcD6_jSdZYL3pKrZlHpT7XyPC4";
        $clientSecret = "MUISquKTaNdkQ0kVJU-0JYNcvZYzBDj6AxEjj8_D3t4";
        $apiKey = "f72d526160b1ec5fbde1bfe0a48b7fb22c22576f";
        
        $options = [
            'clientId' => $clientId, 
            'clientSecret' => $clientSecret,
            'apiKey' => $apiKey,
            'baseUrl' => $baseUrl
        ];

        $k2Service = new K2($options);
        $StkPushService = $k2Service->StkService();
        $accessToken = self::getAccessToken($k2Service);

        $statusOptions = [
            'location' => $location,
            'accessToken' => $accessToken
        ];
        $response = $StkPushService->getStatus($statusOptions);
        return $response;
    }
}
