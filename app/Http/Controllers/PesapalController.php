<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;

/**
 * Pesapal API Package v3
 * @author mosesgitau380@gmail.com
 * @author Moses Gitau
 */

class PesapalController extends Controller
{
    protected $apiUrl, $consumerKey, $consumerSecret, $accessToken = null;

    public function __construct()
    {
        $this->apiUrl = env('PESAPAL_LIVE') ? "https://pay.pesapal.com/v3/api/" 
                                            : "https://cybqa.pesapal.com/pesapalv3/api/";

        $this->consumerKey = env('PESAPAL_LIVE') ? env('PESAPAL_CONSUMER_KEY') : "qkio1BGGYAXTu2JOfm7XSXNruoZsrqEW";
        $this->consumerSecret = env('PESAPAL_LIVE') ? env('PESAPAL_CONSUMER_SECRET') : "osGQ364R49cXKeOYSpaOnT++rHs=";
    }

    public static function accessToken()
    {
        if(env('PESAPAL_LIVE') == false){

            $apiUrl = "https://cybqa.pesapal.com/pesapalv3/api/Auth/RequestToken"; // Sandbox URL
            $consumerKey = "qkio1BGGYAXTu2JOfm7XSXNruoZsrqEW";
            $consumerSecret = "osGQ364R49cXKeOYSpaOnT++rHs=";

        }elseif(env('PESAPAL_LIVE') == true){

            $apiUrl = "https://pay.pesapal.com/v3/api/Auth/RequestToken"; // Live URL
            $consumerKey = env('PESAPAL_CONSUMER_KEY');
            $consumerSecret = env('PESAPAL_CONSUMER_SECRET');

        }else{
            echo "Invalid APP_ENVIRONMENT";
            exit;
        }

        $headers = [
            "Accept: application/json",
            "Content-Type: application/json"
        ];
        $data = [
            "consumer_key" => $consumerKey,
            "consumer_secret" => $consumerSecret
        ];
        $ch = curl_init($apiUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // *! makes this request unsecure. Could not access resource cause pesapal server has an ssl cert problem.
        $response = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $data = json_decode($response);

        $token = $data->token; 
        // $this->accessToken = $token;
        Cache::put('access_token', $token, $seconds = 60);
        return $token;
    }

    public static function registerIPN()
    {
        // Implement IPN registration logic here
        if(env('PESAPAL_LIVE') == false){
            $ipnRegistrationUrl = "https://cybqa.pesapal.com/pesapalv3/api/URLSetup/RegisterIPN";
        }else{
            $ipnRegistrationUrl = "https://pay.pesapal.com/v3/api/URLSetup/RegisterIPN";
        }

        $accessToken = self::accessToken();

        $headers = array(
            "Accept: application/json",
            "Content-Type: application/json",
            "Authorization: Bearer $accessToken"
        );

        $ipn_route = env('PESAPAL_IPN', '/api/pesapal/handle-ipn');
        $data = array(
            "url" => url($ipn_route),
            // "url" => 'https://6fa6-197-232-61-219.ngrok-free.app/api/pesapal/handle-ipn',
            "ipn_notification_type" => "POST" 
        );
        $ch = curl_init($ipnRegistrationUrl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // *! makes this request unsecure. Could not access resource cause pesapal server has an ssl cert problem.
        $response = curl_exec($ch);
        $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $data = json_decode($response);
        // dd($responseCode);

        $ipn_data = [
            'ipn_id' => $data->ipn_id,
            'ipn_url' => $data->url,
        ];

        return $ipn_data;
    }

    public static function handleIPNCallback()
    {
        header("Content-Type: application/json");
        $pinCallbackResponse = file_get_contents('php://input');
        // dd($pinCallbackResponse);
        $logFile = storage_path()."/framework/pesapal/pinCallback.json";
        $log = fopen($logFile, "a");
        fwrite($log, $pinCallbackResponse);
        fclose($log);
    }

    public function getRegisteredIPNs()
    {
        if(env('PESAPAL_LIVE') == false){
            $getIpnListUrl = "https://cybqa.pesapal.com/pesapalv3/api/URLSetup/GetIpnList";
        }else{
            $getIpnListUrl = "https://pay.pesapal.com/v3/api/URLSetup/GetIpnList";
        }

        // $ipn_data = self::registerIPN();
        // dd();
        $accessToken = Cache::get('access_token');

        $headers = array(
            "Accept: application/json",
            "Content-Type: application/json",
            "Authorization: Bearer $accessToken"
        );
        $ch = curl_init($getIpnListUrl);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // *! makes this request unsecure. Could not access resource cause pesapal server has an ssl cert problem.
        $response = curl_exec($ch);
        $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        // dd($response);
        curl_close($ch);
    }

    /**
     * Generate a merchant Reference for OrderRequest
     * @return string
     */
    public static function generateMerchantReference()
    {
        return 'PESAPAL'.rand(1, 1000000000).uniqid();
    }

    /**
     * Initiate a payment to the Merchant
     * @param array $payload
     * @return string $payment_url
     */
    public static function submitOrderRequest($orderPayload =[])
    {
        $ipn_data = self::registerIPN();
        $accessToken = Cache::get('access_token');

        // $merchantreference = 'PESAPAL'.rand(1, 1000000000).uniqid();

        // dd($orderPayload);

        if(env('PESAPAL_LIVE') == false){
          $submitOrderUrl = "https://cybqa.pesapal.com/pesapalv3/api/Transactions/SubmitOrderRequest";
        }else{
          $submitOrderUrl = "https://pay.pesapal.com/v3/api/Transactions/SubmitOrderRequest";
        }
        $headers = array(
            "Accept: application/json",
            "Content-Type: application/json",
            "Authorization: Bearer $accessToken"
        );
        
        // Request payload
        $data = array(
            "id" => $orderPayload['merchantRef'],
            "currency" => "KES",
            "amount" => $orderPayload['amount'],
            "description" => $orderPayload['description'],
            "callback_url" => $orderPayload['callbackurl'],
            "redirect_mode" => $orderPayload['redirect_mode'],
            "notification_id" => $ipn_data['ipn_id'],
            "branch" => $orderPayload['branch'],
            "billing_address" => array(
                "email_address" => $orderPayload['email_address'],
                "phone_number" => $orderPayload['phone_number'],
                "country_code" => "KE",
                "first_name" => $orderPayload['first_name'],
                "middle_name" => $orderPayload['middle_name'],
                "last_name" => $orderPayload['last_name'],
                "line_1" => "IREN Growthpad",
                "line_2" => "",
                "city" => "",
                "state" => "",
                "postal_code" => "",
                "zip_code" => ""
            )
        );
        $ch = curl_init($submitOrderUrl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // *! makes this request unsecure. Could not access resource cause pesapal server has an ssl cert problem.
        $response = curl_exec($ch);
        // dd($response);
        $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $data = json_decode($response);

        $payment_url = $data->redirect_url;
        return $payment_url;
    }

    /**
     * Check the status for the payment and handle the response accordingly
     */
    public static function handleResponseCallback()
    {
        //?OrderTrackingId=1c298d87-ef37-4e7c-ab33-de3dfccce94d
        //&OrderMerchantReference=92582762768768274

        $OrderTrackingId = $_GET['OrderTrackingId'];
        $OrderMerchantReference = $_GET['OrderMerchantReference'];
        $accessToken = Cache::get('access_token');

        if(env('PESAPAL_LIVE') == false){
        $getTransactionStatusUrl = "https://cybqa.pesapal.com/pesapalv3/api/Transactions/GetTransactionStatus?orderTrackingId=$OrderTrackingId";
        }else{
        $getTransactionStatusUrl = "https://pay.pesapal.com/v3/api/Transactions/GetTransactionStatus?orderTrackingId=$OrderTrackingId";
        }
        $headers = array(
            "Accept: application/json",
            "Content-Type: application/json",
            "Authorization: Bearer $accessToken"
        );
        $ch = curl_init($getTransactionStatusUrl);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // *! makes this request unsecure. Could not access resource cause pesapal server has an ssl cert problem.
        $response = curl_exec($ch);
        $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return $response;
    }
}