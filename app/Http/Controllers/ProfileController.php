<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function edit($id = null)
    {
        // Retrieve user information and display on profile page
        $user = User::find($id ?? auth()->user()->id);
        return view('cookbook/edit-profile', compact('user'));
    }

    public function update(Request $request, $id = null)
    {
        // dd($request);
        // $request = request()->only(['name', 'pick_up_address', 'email', 'telephone', 'current_password', 'password']);
        $validated = $request->validate([
            'name'                  =>'required|string|max:200',
            'custom_address'      =>'nullable|string|max:255',
            'email'                 =>'required|string|email|max:255',
            'telephone'             =>'required|string|max:255',
            'password'              =>'nullable|string|min:6|confirmed',
            'password_confirmation'      =>'nullable|required_with:password|string|min:6',
        ]);

        $data = [
            'name' => $validated['name'],
            'pick_up_address' => isset($request->pick_store) ? 'Hurlingham Store' : $validated['custom_address'],
            'email' => $validated['email'],
            'telephone' => $validated['telephone'],
        ];

        isset($validated['password']) ? $data['password'] = bcrypt($validated['password']) : false;
        // Update user information
        $user = User::find($id ?? auth()->user()->id);
        $user->update($data);
        return isset($request->backUrl) ? redirect()->route($request->backUrl) : redirect()->back()->with('success', 'Profile updated successfully!');
    }
}