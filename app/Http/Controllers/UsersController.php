<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UserRequest;
use App\Models\User;
use App\Models\Ad;
use App\Models\Order;
use Illuminate\Http\Request;

class UsersController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($SPs = false)
    {
        $accounts = (@$_GET['q']) ?
            User::where('name', 'LIKE', '%' . @$_GET['q'] . '%')
            ->orWhere('business_name', 'LIKE', '%' . @$_GET['q'] . '%')
            ->orWhere('county', 'LIKE', '%' . @$_GET['q'] . '%')
            ->paginate() : User::paginate();

        return view('users.list', [
            'accounts' => $accounts
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('users.show', [
            'account' => User::find($id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('users.edit', [
            'account' => User::find($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        User::find($id)->update($request->only(['name', 'email', 'telephone', 'privilege', 'gender', 'credits', 'county', 'business_name', 'business_category']));

        $request->session()->flash('successbox', ['User account successfully updated']);

        return redirect()->route('users.show', ['id' => $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);

        # Delete user ads and orders
        $ads = Ad::where('publisher_id', $id)->get();
        $ads->each(function ($ad) {
            Order::where('ad_id', $ad->id)->delete();
        });

        $user->delete();

        request()->session()->flash('successbox', ['User account successfully deleted']);

        return redirect()->route('users.index');
    }

    function listSPs()
    {
        $accounts = User::where('privilege', 'SP')->paginate();

        return view('users.list', [
            'accounts' => $accounts
        ]);
    }

    function logout()
    {
        Auth::logout();

        return redirect('/');
    }

    function youCanLogIn()
    {
        return response()->json([
            'status'  => 'SUCCESS!',
            'message' => 'You have successfully changed your password. Please close this window and try logging in with your new password'
        ]);
    }

    function previewCookbook()
    {

        $AUTH_USER = 'iren';
        $AUTH_PASS = 'DYU5c7ak#vQc8c72kF&^lr5*Y3te7D';

        header('Cache-Control: no-cache, must-revalidate, max-age=0');
        $has_supplied_credentials = !(empty($_SERVER['PHP_AUTH_USER']) && empty($_SERVER['PHP_AUTH_PW']));
        $is_not_authenticated = (!$has_supplied_credentials ||
            $_SERVER['PHP_AUTH_USER'] != $AUTH_USER ||
            $_SERVER['PHP_AUTH_PW']   != $AUTH_PASS
        );
        if ($is_not_authenticated) {
            header('HTTP/1.1 401 Authorization Required');
            header('WWW-Authenticate: Basic realm="Access denied"');
            echo '<h1>Access denied!</h1>';
            exit;
        }

        $product = config('cookbook.products')[0];
        $file = storage_path() . '/app/' . $product['file'];

        return response()->file($file);
    }
}
