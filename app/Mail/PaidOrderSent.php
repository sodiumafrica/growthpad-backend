<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PaidOrderSent extends Mailable
{
    use Queueable, SerializesModels;

    public $order;
    public $order_items;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order, $order_items)
    {
        $this->order = $order;
        $this->order_items = json_decode($order_items, true);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('New order [Paid]!')
                    ->cc('noreply@irenkenya.com')
                    ->markdown('emails.paid-order-received');
    }
}
