@extends('layout.recipe')

@section('content')

    <!-- slider_area_start -->
    <div class="slider_area">
        <div class="single_slider  d-flex align-items-center"
             style="background-image: url('{{ asset('recipe/img/purchase.jpg') }}')">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-xl-8 ">
                        <div class="slider_text text-center">
                            <div class="text">
                                <h3>
                                    Your shopping cart
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- slider_area_end -->

    @if (session('errorbox'))
        <div class="d-block bg-danger text-center text-light h3">
        {{ session('errorbox') }}
        </div>
    @endif

    <div class="recepie_videoes_area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    @if ($items->count())
                        <table class="table table-bordered">
                            <tr>
                                <th>Product Name</th>
                                <th>Price</th>
                                <th>Quantity</th>
                            </tr>
                            @php
                                $totalKES = 0;
                                $totalEUR = 0;

                                $grandTotal = 0;
                                $totalQuantity = 0;
                                // dd($items);
                            @endphp
                            @foreach ($items as $item)
                                @php
                                    // dd($item['total']);
                                    $raw = $item->rawId();

                                    if ($item->type == 'vegetable') {
                                        $product = config('cookbook.veges')[$item->id];
                                        $totalKES = $item['total'];
                                    }

                                    if ($item['type'] == 'recipe') {
                                        $product = config('cookbook.products')[$item->id];
                                        // $totalKES = $totalKES + $item['price']['en'];
                                        $totalEUR = $totalEUR + $item['price']['de'];
                                        $totalKES = $item['total'];
                                    }

                                    $grandTotal = $grandTotal + $totalKES;
                                    $totalQuantity = $totalQuantity + $item['qty'];

                                @endphp

                            @if ($item['type'] == 'recipe')
                                <tr>
                                    <td>{{ $product['name'][app()->getLocale('en')] }}</td>
                                    <td>
                                        {{ (app()->getLocale('en') == 'de') ? 'EUR' : 'KES' }}
                                        {{ $product['price'][app()->getLocale('en')] }}
                                    </td>
                                    <td>
                                        1

                                        <span class="float-right">
                                            <a href="/cookbook/cart/remove/{{ $raw }}" class="text-danger">Remove Item</a>
                                        </span>
                                    </td>
                                </tr>
                            @endif

                            @if ($item['type'] == 'vegetable')
                                <tr>
                                    <td>{{ $item['name'] }}</td>
                                    <td>
                                        {{ (app()->getLocale('en') == 'de') ? 'EUR' : 'KES' }}
                                        {{ $item['price'] }}
                                    </td>
                                    <td>
                                        {{ $item['qty'] }}

                                        <span class="float-right">
                                            <a href="/cookbook/cart/remove/{{ $raw }}" class="text-danger">Remove Item</a>
                                        </span>
                                    </td>
                                </tr>
                            @endif
                            @endforeach
                            <tr>
                                <th>Grand Total</th>
                                <th>
                                    {{ (app()->getLocale('en') == 'de') ? 'EUR' : 'KES' }}
                                    {{-- {{ (app()->getLocale('en') == 'de') ? $totalEUR : $totalKES }} --}}
                                    {{$grandTotal}}
                                </th>
                                <th>
                                    {{$totalQuantity}}

                                    <span class="float-right">
                                        <a href="/cookbook/cart/remove/all" class="text-danger">Remove everything</a>
                                    </span>
                                </th>
                            </tr>
                        </table>
                        <br>

                        <div class="col-12 text-center">
                            {{-- @if(app()->getLocale() == 'en') --}}
                                <a href="/cookbook/cart/checkout"
                                class="btn btn-success btn-lg mt-3 mb-5">
                                <i class="fa fa-cart-plus fa-fw" aria-hidden="true"></i>Proceed to Checkout
                                </a>                               
                            {{-- @endif --}}
                        </div>

                    @else
                        <div class="alert alert-danger">
                            You have not added anything to the cart
                        </div>

                        <a href="/cookbook" class="btn btn-success btn-lg btn-block">
                            Shop items here
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

