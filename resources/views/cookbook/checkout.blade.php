@extends('layout.recipe')

{{-- CSS --}}

<style>
    body{
    background:#eee;
    }
    .card {
        box-shadow: 0 20px 27px 0 rgb(0 0 0 / 5%);
    }
    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 0 solid rgba(0,0,0,.125);
        border-radius: 1rem;
    }
    .text-reset {
        --bs-text-opacity: 1;
        color: inherit!important;
    }
    a {
        color: #5465ff;
        text-decoration: none;
    }

    .container-fluid {
        margin-top: 6rem;
    }

    #paybill[disabled] {
        cursor: not-allowed;
    }
    #paybill[disabled] a{
        pointer-events: none;
    }
</style>

@section('content')

<div class="container-fluid">
    
    @if (session('errorbox'))
        <div class="d-block bg-danger text-center text-light h3">
        {{ session('errorbox') }}
        </div>
    @endif

    <div class="container">
    <!-- Title -->
    <div class="d-flex justify-content-between align-items-center py-3">
        <h2 class="h5 mb-0">
            <a href="/cookbook" class="text-primary">
                <i class="fa fa-arrow-left mr-1" aria-hidden="true"></i>Continue Shopping
            </a>
        </h2>
    </div>

    <!-- Main content -->
    <div class="row">
        <div class="col-lg-8">
        <!-- Details -->
        <div class="card mb-4">
            <div class="card-body">
            {{-- <div class="mb-3 d-flex justify-content-between">
                <div>
                    <span class="me-3">22-11-2021</span>
                    <span class="me-3">#16123222</span>
                    <span class="me-3">Visa -1234</span>
                    <span class="badge rounded-pill bg-info">SHIPPING</span>
                </div>
                <div class="d-flex">
                    <button class="btn btn-link p-0 me-3 d-none d-lg-block btn-icon-text"><i class="bi bi-download"></i> <span class="text">Invoice</span></button>
                    <div class="dropdown">
                        <button class="btn btn-link p-0 text-muted" type="button" data-bs-toggle="dropdown">
                        <i class="bi bi-three-dots-vertical"></i>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-end">
                        <li><a class="dropdown-item" href="#"><i class="bi bi-pencil"></i> Edit</a></li>
                        <li><a class="dropdown-item" href="#"><i class="bi bi-printer"></i> Print</a></li>
                        </ul>
                    </div>
                </div>
            </div> --}}
            <table class="table table-borderless">
                {{-- @dd($items) --}}
                <tbody>
                    @foreach ($items as $item)    
                        <tr>
                            <td>
                            <div class="d-flex mb-2">
                                <div class="flex-shrink-0 mr-1">
                                    <img src="{{ $item['picture'] }}" alt="" width="35" class="img-fluid">
                                </div>
                                <div class="flex-lg-grow-1 ms-3">
                                    <h6 class="small mb-0"><a href="#" class="text-reset">{{$item->name}}</a></h6>
                                    <span class="small">Unit Price: {{$item->price}}</span>
                                </div>
                            </div>
                            </td>
                            <td>{{$item->qty}}</td>
                            <td class="text-end">{{$item->total}}</td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                {{-- <tr>
                    <td colspan="2">Shipping</td>
                    <td class="text-end">$20.00</td>
                </tr> --}}
                {{-- <tr>
                    <td colspan="2">Discount (Code: NEWYEAR)</td>
                    <td class="text-danger text-end">-$10.00</td>
                </tr> --}}
                <tr class="fw-bold">
                    <td colspan="2">TOTAL</td>
                    <td class="text-end">{{$grandTotal}}</td>
                </tr>
                </tfoot>
            </table>
            </div>
        </div>
        <!-- Payment -->
        <div class="card mb-4">
            <div class="card-body">
                <h3 class="h6">Payment Method</h3>
            <div class="row">
                <div class="col-lg-6" id="paybill" title="Mpesa Till">
                {{-- <p>Visa -1234 <br>
                Total: $169,98 <span class="badge bg-success rounded-pill">PAID</span></p> --}}
                    <a href="/cookbook/cart/checkout/{{$paymentId}}"
                        class="btn btn-success btn-lg mt-3 mb-5 @if (request()->has('till')) d-none @endif">
                        Via M-Pesa Till
                    </a>
                    <ul class="small list-group list-group-flush @if (request()->has('till')) d-none @endif">
                        <li class="list-group-item">M-pesa (Buy Goods Till)</li>
                        {{-- <li class="list-group-item">Till: K851681</li> --}}
                        <li class="list-group-item">
                            Phone Number: {{auth()->user()->telephone}} <br>
                            <span style="font-size: 10px;">*You can change your number in the <a style="color: rgb(57, 57, 230)" href="/profile/edit?from=checkout">edit profile page</a></span>
                        </li>
                    </ul>
                    <div class="d-none @if (request()->has('till')) d-block @endif align-items-center processPayment">
                        <div class="alert alert-info frmChecking">
                            We are checking for a payment from the number ({{ auth()->user()->telephone }})
                        </div>
                        <div class="text-center frmLoader">
                            <img src="{{ asset('images/loading.gif') }}" width="200px">
                        </div>
                    </div>

                    <div class="d-none align-items-center paymentError">
                        <div class="alert alert-danger errDetected">
                            Something went wrong while processing your payment! Click below to try again or choose another payment method.
                        </div>

                        <a href="/cookbook/cart/checkout" class="btn btn-success btn-sm mt-3">Choose a payment method</a>
                    </div>

                    <a href="#" class="btn btn-success btn-sm d-none btnComplete">Complete Checkout</a>
                </div>
                <div id="pesapalframe" class="col-lg-6" @if (request()->has('till')) style="cursor: no-drop; display: none;" @endif>
                    <a href="#!" id="place_order_trigger"  data-toggle="modal" data-target="#pesapal"
                    @if (request()->has('till')) style="pointer-events:none;" @endif
                        class="btn btn-primary btn-lg mt-3 mb-5">
                        <i class="fa fa-credit-card fa-fw"></i>Via Pesapal
                    </a>
                    <ul class="small list-group list-group-flush">
                        <li class="list-group-item">M-pesa</li>
                        <li class="list-group-item">Airtel Money</li>
                        <li class="list-group-item">Visa</li>
                        <li class="list-group-item">Pesapal e-wallet</li>
                    </ul>

                    <!-- checkout pesapal iframe -->
                    @include('cookbook.pesapal-iframe')
                </div>
            </div>
            </div>
        </div>
        </div>
        <div class="col-lg-4">
        <!-- Customer Notes -->
        {{-- <div class="card mb-4">
            <div class="card-body">
            <h3 class="h6">Customer Notes</h3>
            <p>Sed enim, faucibus litora velit vestibulum habitasse. Cras lobortis cum sem aliquet mauris rutrum. Sollicitudin. Morbi, sem tellus vestibulum porttitor.</p>
            </div>
        </div> --}}
        <div class="card mb-4">
            <!-- Shipping information -->
            <div class="card-body">
                <h3 class="h6">Shipping Information</h3>
                <hr>
                <h3 class="h6">Address</h3>
                <address>
                    <strong>{{ auth()->user()->name }}</strong><br>
                    <span class="small">Delivery/Pick-up</span><br>
                    <span id="address">{{auth()->user()->pick_up_address}}</span><br>
                    <abbr title="Phone">P:</abbr> {{ auth()->user()->telephone }}
                </address>
                <hr>
                <div class="mb-2">
                    <span style="font-size:14px; color:rgb(160, 160, 160); display:block;">*(optional) set custom address for this order</span>

                    <a href="#!" id="place_order_trigger"  data-toggle="modal" data-target="#custom_address"
                        {{-- data-item="{{ @$grandTotal }}"
                        data-array="{{ $items }}" --}}
                        class="btn btn-outline-primary btn-sm">
                        <i class="fa fa-map-marker fa-fw"></i>Set custom address
                    </a>
                </div>
                <!-- custom address modal -->
                @include('cookbook.modals.custom-address')
            </div>
        </div>
        </div>
    </div>
    </div>
  </div>

  @if (request()->has('status'))
  <script>
    document.addEventListener("DOMContentLoaded", (e) => {
        let paymentid = {{request()->get('p')}}
    
        checkStatus(paymentid)
    
        checkPaymentInterval = setInterval(checkStatus(paymentid), 30000)
    
        function checkStatus(paymentid)
        {
            // Place your code to check status here
            fetch('/cookbook/cart/checkPaymentStatus/'+paymentid, )
            .then(response => response.json())
            .then(data => {
                // console.log(data);
                if (data.status == 'complete') {
                    $('.btnComplete').removeClass('d-none')
                    $(".btnComplete").attr("href", "/cookbook/cart/mpesa/finish/"+paymentid+"/"+data.ref)
                    $('.processPayment').removeClass('d-block')
    
                    clearInterval(checkPaymentInterval)
                }else if(data.status == 'failed') {
                    $('.paymentError').removeClass('d-none')
                    $('.processPayment').removeClass('d-block')
                    console.log(data.error)
                    clearInterval(checkPaymentInterval)
                }
            })
            .catch(error => console.error(error));
        }
    });
  </script>
@endif