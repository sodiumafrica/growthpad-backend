@extends('layout.recipe')

@section('content')

{{-- <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"> --}}
<div class="container bootstrap snippets bootdeys" style="margin-top: 8rem">
    @if (session()->has('success'))
        <div class="col-12 alert alert-success">
            {{ session()->get('success') }}
        </div>
    @endif
<div class="row">
  <div class="col-xs-12 col-sm-9">
    <form class="form-horizontal" method="POST" action="{{ url('/profile/update') }}">
        @method('PUT')
        @csrf
        {{-- <div class="panel panel-default">
          <div class="panel-body text-center">
           <img src="https://bootdey.com/img/Content/avatar/avatar6.png" class="img-circle profile-avatar" alt="User avatar">
          </div>
        </div> --}}
      <div class="panel panel-default">
        <div class="panel-heading">
        <h4 class="panel-title">User info</h4>
        </div>
        <div class="panel-body">
          <div class="form-group">
            <label class="col-sm-2 control-label">Full name</label>
            <div class="col-sm-10">
              <input type="text" name="name" value="{{ old('name') ?? $user->name }}" class="form-control">
            </div>
            @if ($errors->has('name'))
                <span class="help-block text-danger">
                    <i class="fa fa-exclamation-circle fa-fw"></i> {{ $errors->first('name') }}
                </span>
            @endif
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">E-mail</label>
            <div class="col-sm-10">
              <input type="email" name="email" value="{{ old('email') ?? $user->email }}" class="form-control">
            </div>
            @if ($errors->has('email'))
                <span class="help-block text-danger">
                    <i class="fa fa-exclamation-circle fa-fw"></i> {{ $errors->first('email') }}
                </span>
            @endif
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label">Mobile number</label>
          <div class="col-sm-10">
            <input type="tel" name="telephone" value="{{ old('telephone') ?? $user->telephone }}" class="form-control">
          </div>
          @if ($errors->has('telephone'))
              <span class="help-block text-danger">
                  <i class="fa fa-exclamation-circle fa-fw"></i> {{ $errors->first('telephone') }}
              </span>
          @endif
        </div>
        </div>
      </div>

      <div class="panel panel-default">
        <div class="panel-heading">
        <h4 class="panel-title">Default Address</h4>
        </div>
        <h6 style="font-size: 14px; color:#1f943a;">*This is used when shipping out orders if no custom address is set on checkout</h6>
        <div class="panel-body">

          <div class="form-group">
            <label class="col-sm-3 control-label">1. Pick Up address</label>
            <label for="pick_store" class="d-flex justify-content-start align-items-center">
              <input type="checkbox" {{ old('pick_store') || $user->pick_up_address == 'Hurlingham Store' ? 'checked' : '' }} name="pick_store" id="pick_store" style="width: 15px; height: 15px; margin: 0 10px;"> 
              <span class="mr-2">Pick up at the Hurlingham store</span>
          </label>
            @if ($errors->has('pick_store'))
                <span class="help-block text-danger">
                    <i class="fa fa-exclamation-circle fa-fw"></i> {{ $errors->first('pick_store') }}
                </span>
            @endif
          </div>
          
          <div class="form-group">
            <label class="col-sm-3 control-label">2. Delivery address</label>
            <div class="col-sm-10">
              <input type="text" id="custom_address" {{ old('pick_store') || $user->pick_up_address == 'Hurlingham Store' ? 'disabled' : '' }} name="custom_address" value="{{ old('custom_address') ?? ($user->pick_up_address != 'Hurlingham Store' ? $user->pick_up_address : '') }}" class="form-control">
            </div>
            @if ($errors->has('pick_up_address'))
                <span class="help-block text-danger">
                    <i class="fa fa-exclamation-circle fa-fw"></i> {{ $errors->first('pick_up_address') }}
                </span>
            @endif
          </div>
        </div>
      </div>

      <div class="panel panel-default">
        <div class="panel-heading">
        <h4 class="panel-title">Security (optional)</h4>
        </div>
        <h6 style="font-size: 14px; color:#666666;">*You can change your current password here</h6>
        <div class="panel-body">
          <div class="form-group">
            <label class="col-sm-2 control-label">New password</label>
            <div class="col-sm-10">
              <input type="password" name="password" class="form-control">
            </div>
            @if ($errors->has('password'))
                <span class="help-block text-danger">
                    <i class="fa fa-exclamation-circle fa-fw"></i> {{ $errors->first('password') }}
                </span>
            @endif
          </div>
          <div class="form-group">
            <label class="col-sm-6 control-label">Confirm password</label>
            <div class="col-sm-10">
              <input type="password" name="password_confirmation" class="form-control">
            </div>
            @if ($errors->has('password_confirmation'))
                <span class="help-block text-danger">
                    <i class="fa fa-exclamation-circle fa-fw"></i> {{ $errors->first('password_confirmation') }}
                </span>
            @endif
          </div>
          
          {{-- Hidden fields --}}
          <input type="hidden" name="backUrl" value="{{ request('from') ?? null }}">

          <div class="form-group">
            <div class="col-sm-10 col-sm-offset-2">
              <button type="submit" class="btn btn-primary">Submit</button>
              <button type="reset" class="btn btn-secondary">Reset</button>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
</div>

<script>
    //fields
    const pickStoreCheckbox = document.getElementById('pick_store');
    const pickCustomInput = document.getElementById('custom_address');

    //event listeners
    pickStoreCheckbox.addEventListener('change', function() {
        pickCustomInput.disabled = this.checked;

        // if (this.checked) {
        //     pickCustomInput.value = '';
        // }
    });
</script>

@endsection
