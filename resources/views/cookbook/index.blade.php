@extends('layout.recipe')

<!-- Styles -->
<style>
    .carousel-indicators li {
        list-style: none !important;
    }

    .about_wrapper {
        position: relative;
        padding: 5px !important;
    }

    .about_wrapper img {
        position:absolute;
        right: 0;
        top: 0;
        width: 100px;
        height: 100px;
        z-index: 20;
    }
    .about_header{
        position: relative;
        padding: 10px 15px;
        background-color: #19c942;
        color: white;
        border-radius: 8px;
        font-size: 35px;
        font-weight: 800;
        letter-spacing: 4px;
    }

    .about_body {
        padding: 20px 15px;
    }
    .about_body p {
        font-size: 20px;
        font-weight: 400;
        letter-spacing: 2px;
        line-height: 20px;
        color: #000000;
        padding: 10px 0;
    }

    .value_wrapper{
        position: relative;
        padding: 10px 15px;
        background-color: #19c942;
        color: white;
        border-radius: 8px;
        font-size: 35px;
        font-weight: 800;
        letter-spacing: 4px;
    }

    .values_wrapper .values_body{
        top: 0;
        left: 0;
        width: 100%;
        height: 99%;
        margin: 10px auto;
        padding: 15px 20px;
        font-size: 25px;
        font-weight: 400;
        letter-spacing: 1.5px;
    }

    .values_body {
        display: flex;
        align-items: center;
        justify-content: center;
        font-size: 18px !important;
        color: rgb(0, 0, 0);
    }

    .values_wrapper div ul li{
        margin: 5px 10px;
    }

    .vision_wrapper img {
        position:absolute;
        padding-right: 25px;
        bottom: 0;
        width: 100px;
        height: 100px;
        z-index: 20;
    }

    .vision_body {
        text-align: center;
        font-size: 20px;
        font-weight: 400;
        letter-spacing: 1.5px;
        line-height: 15px;
    }

    .vision_body span {
        font-weight: 800;
        color:#1f943a;
        margin: 8px 0;
    }

    .vision_body div {
        width: 100%;
        text-align: center;
        font-weight: 400;
        letter-spacing: 1.2px;
        line-height: 30px;
        margin: 10px 0 20px 0;
    }

    /* Responsive settings */
    @media screen and (max-width: 500px){
        .about_header{
            font-size: 20px;
            font-weight: 400;
            letter-spacing: 2px;
            line-height: 15px;
        }
        .about_body p {
            font-size: 14px;
            font-weight: 400;
            letter-spacing: 1.2px;
            line-height: 15px;
        }

        .about-managu-img {
            width: 150px;
            height: auto;
        }

    }
</style>
<!-- End of Styles -->

@section('content')

<div class="container" style="padding-top: 22px;"> 
  <div class="row">
    <div class="col-12">
    <div id="carousel" class="carousel slide pt-5" data-ride="carousel" data-interval='15000'>
        <ul class="carousel-indicators">
            @php($active = true)
            @foreach($slides as $image)
                <li data-target="#carousel" data-slide-to="0" class="@if($active) active @endif"></li>
                @php($active = false)
            @endforeach
        </ul>
        <div class="carousel-inner">
            @php($active = true)
            @foreach($slides as $image)
                <div class="carousel-item @if($active) active @endif">
                    <img class="d-block w-100" src="{{ $image }}">
                </div>
                @php($active = false)
            @endforeach
        </div>
        <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
  </div>
 </div>    
</div> 

    <!-- recepie_videos   -->
    <div class="recepie_videoes_area">
        <div class="container">
            <div class="recepie_info">
                <div class="row">
                    @if(app()->getLocale() == 'en')
                        <div class="about_wrapper col-12 col-lg-6" id="about">
                            <img src="images/leaf.png" alt="Leaf">
                            <div class="about_header">
                                <span>About</span>
                                <p>IREN Growthpad LTD</p>
                            </div>
                            <div class="about_body text-justify text-left">
                                <p>
                                    IREN Growthpad is on a mission to revolutionize access to nutritious African 
                                    Indigenous Vegetables (AIVs) products.
                                </p>
                                <p>
                                    With a vision to power our clients wellness by providing convenient , 
                                    nutrient-dense African Indigenous Vegetable products with extended shelf-life.
                                </p>
                                <p>
                                    Registered in Kenya 2022,  we have a processing site in Shianda, Kakamega 
                                    county and the Sales and HQ in Huligham Nairobi county.
                                </p>
                            </div>
                        </div>   
                        <div class="col-12 col-md-6 d-none d-lg-block position-relative">
                            <img src="images/dried-managu.png" alt="Dried Managu" class="position-absolute about-managu-img" style="max-width:350px; height: auto; padding-top:80px; right: 10px; z-index:20;">
                            <img src="images/bg-about-values.png" alt="" class="img-fluid">    
                        </div> 
                        
                            <!-- Force next columns to break to new line -->
                        <div class="w-100"></div>

                        <div class="col-12 col-sm-6 position-relative values_wrapper" id="values">
                            <div class="position-absolute top-0 left-0 values_body">
                                <div>
                                    <h4 style="font-size: 25px; font-weight: 600; color: rgb(4, 114, 23); margin-right: 5px;">Our Core Values:</h4>
                                    <ul>
                                        <li>Innovation</li>
                                        <li>Relationship building</li>
                                        <li>Quality</li>
                                        <li>Integrity</li>
                                    </ul>
                                </div>
                            </div>
                            <img src="images/bg-about-values.png" alt="" class="img-fluid">    
                        </div> 
                        <div class="vision_wrapper col-12 col-sm-6">
                            <img src="images/leaf.png" alt="Leaf" class="d-none d-lg-block">
                            <div class="about_header p-4 mb-4">
                                <span>Vision and Mission</span>
                            </div>
                            <div class="vision_body">
                                <span>Vision</span>
                                <div>To power our clients wellness</div>
                                
                                <span>Mission</span>
                                <div>To revolutionize access to nutritious African Indigenous Vegetables products.</div>
                            </div>
                        </div>   
                    @else
                        No translation available
                    @endif
                </div>
            </div>
        </div>
    </div>

    <!-- Order vegetables  -->
    <div class="recepie_area" id="order_vegetables">
        <div class="container">
            <div class="row pb-5">
                <div class="col-12 text-center pt-2">
                    <a name="shop"></a>
                    <h2 style="line-height: 40px;">
                        <i class="fa fa-star-o fa-fw"></i>
                        Order dried vegetables
                        <i class="fa fa-star-o fa-fw"></i>
                    </h2>
                </div>
                @if (session()->has('successbox'))
                    <div class="col-12 alert alert-success">
                        {{ session()->get('successbox')[0] }}
                    </div>
                @endif
                <div class="col-12">
                    
                </div>
            </div>
            <div class="row">
                @foreach($veges as $key => $product)
                    <div class="col-xl-4 col-lg-4 col-md-6">
                        <div class="card card-body p-0 mb-5">
                            <div class="single_recepie text-center">
                                <div class="recepie_thumbx bg-dark position-relative">
                                    <img src="{{ $product['product_bag'] }}" class="img-fluid" alt="">
                                    <img src="{{ $product['product_bottle'] }}" class="img-fluid position-absolute" style="z-index: 30; top: 80px; left: 0;">
                                </div>
                                <h4 class="product-name mt-3 mb-3">{{ @$product['name'].' / '.@$product['engName'] }}</h4>
                                <div>
                                @foreach ( @$product['price'] as $size => $price)
                                    <span>{{ @$size }} - KES {{ @$price['retail'] }}</span>
                                @endforeach
                                </div>
                                {{-- <a href="#!"  data-toggle="modal" data-target="#exampleModal"
                                data-item="{{ @$product['name'] }}"
                                   class="line_btn">
                                    Buy Now
                                </a> --}}
                                <a href="/cookbook/veges/{{ encrypt($key) }}"
                                   class="line_btn">
                                    Buy Now
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
                <!-- Button trigger modal -->

                <!-- Order vegetables form -->
                @include('cookbook.order-veges')
            </div>

            {{-- <div class="col-12" style="text-align: center;">
                <span style="display: block;">(All prices exclude shipment cost)</span>
                <a target="_blank" href="recipe\IREN Growthpad Ltd  Product Price List.pdf">Product Price List</a>
            </div> --}}
        </div>
    </div>

    <!-- recepie_area_start  -->
    <div class="recepie_area" id="purchase_recepies">
        <div class="container">
            <div class="row pb-5">
                <div class="col-12 text-center pt-2">
                    <a name="shop"></a>
                    <h2 style="line-height: 40px;">
                        <i class="fa fa-star-o fa-fw"></i>
                        {{ __('cookbook.purchase_our') }}
                        <i class="fa fa-star-o fa-fw"></i>
                    </h2>


                    <div class="row mt-5">
                        <div class="col text-right">
                            <a class="text-primary" href="/cookbook?filter=vegan#shop">{{ __('cookbook.vegan') }}</a>
                        </div>
                        <div class="col text-left">
                            <a class="text-primary" href="/cookbook?filter=meat#shop">{{ __('cookbook.meat') }}</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach($products as $key => $product)

                    @if($product['name'][app()->getLocale()])
                        <div class="col-xl-4 col-lg-4 col-md-6">
                            <div class="card card-body p-0 mb-5">
                                <div class="single_recepie text-center">
                                    <div class="recepie_thumbx">
                                        <img src="{{ (strlen($product['picture'])) ? asset($product['picture']) : '/recipe/products/noimage.png' }}"
                                             class="img-fluid">
                                    </div>
                                    <h4 class="product-name mt-3">{{ @$product['name'][app()->getLocale()] }}</h4>
                                    <p class="text-center">
                                        {{ (app()->getLocale() == 'en') ? 'Ksh' : '€' }}
                                        {{ @number_format($product['price'][app()->getLocale()]) }}
                                    </p>
                                    <a href="/cookbook/display/{{ encrypt($key) }}"
                                       class="line_btn">{{ __('cookbook.buy_this') }}</a>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach

                <div class="col-12">
                    <div class="alert alert-success text-center">
                        You will be asked to login or register a Growthpad account before purchasing items
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="bg-light mb-5" id="videos">
        <div class="container">
            <div class="row pt-5 pb-5 mb-5">
                <div class="col-12 mb-3">
                    <h1>Videos</h1>
                </div>
                <div class="col-12 col-lg-6">
                    <div class="videos_thumb">
                        <iframe width="100%" height="315"
                                src="https://players.brightcove.net/1628196552001/default_default/index.html?videoId=6202567282001"
                                frameborder="0"
                                allowfullscreen></iframe>
                    </div>
                </div>
                <div class="col-12 mt-3 mt-lg-0 col-lg-6">
                    <div class="videos_thumb">
                        <iframe width="100%" height="315" src="https://www.youtube-nocookie.com/embed/videoseries?si=nVkzP7xby5mlT3o7&amp;list=PL7xugFeVHXWelo38FACH0MUASzBfsQsG6" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <!-- download_app_area -->
    <div class="download_app_area">
        <div class="container">
            <div class="row align-items-center">
                {{-- <div class="col-lg-6 col-md-6">
                    <div class="videos_thumb"> --}}
                        {{-- <iframe width="100%" height="315" src="https://www.youtube-nocookie.com/embed/4JGypwuPzr4"
                                frameborder="0"
                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                allowfullscreen></iframe> --}}
                        {{-- <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/videoseries?si=nVkzP7xby5mlT3o7&amp;list=PL7xugFeVHXWelo38FACH0MUASzBfsQsG6" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe> --}}
                    {{-- </div> --}}
                {{-- </div> --}}
                <div class="col-12">
                    <div class="download_text" id="download_text">
                        @if(app()->getLocale() == 'en')
                            <h4 style="font-size: 30px;">Get your copy of Chakula Chetu today!</h4>
                            <p>
                                Full Chakula Chetu recipe book is available in soft copy at 40 euros. Individual soft
                                copy recipes available at 2 euros
                            </p>
                            <p class="mb-2">
                                On purchase, you will receive the recipes in a PDF file format for your daily use. Hard
                                copy versions of the recipe book are available only in Kenya at the following contact
                                details:
                            </p>
                            <p>Contact Us</p>
                            <ul class="pl-5 pt-3 pb-3">
                                <li>IREN Growthpad Ltd</li>
                                <li>Shianda Off Kakamega – Mumias Road</li>
                                <li>P.O Box 135 – GPO 00100</li>
                                <li>Twitter @IRENGrowthpad</li>
                                <li>Facebook IREN Growthpad</li>
                                <li>Instagram @iregrowthpad_ke</li>
                                <li>Whatsapp chat with us 0798802824</li>
                                <li>Email growthpad@irenkenya.com</li>
                            </ul>
                            <p>
                                Enjoy your cooking!
                            </p>
                        @else
                            <h4 style="font-size: 30px;">Hol dir noch heute deine Ausgabe von Chakula Chetu!</h4>
                            <p class="mb-2">
                                Hole Dir noch heute Deine Ausgabe von Chakula Chetu!
                                Das gesamte „Chakula Chetu“-Kochbuch ist für 40€ in digitaler Form erhältlich. Einzelne
                                Rezepte erhältst Du ebenfalls digital für 2€ pro Rezept.
                                Nach Deiner Bestellung wirst Du die Rezepte in Form einer PDF-Datei für Deinen täglichen
                                Gebrauch erhalten. Gebundene Ausgaben des Kochbuchs sind nur in Kenia verfügbar, bei
                                Interesse kontaktiere uns gerne.

                            </p>
                            <ul class="pl-5 pt-3 pb-3">
                                <li>
                                    Schreibe uns eine E-Mail an chepchirchir@irenkenya.com
                                </li>
                                <li>
                                    Oder rufe uns an unter +254 798 802 818
                                </li>
                            </ul>
                            <p>
                                Viel Spaß beim Kochen!
                            </p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/ download_app_area -->

    <style>
        p {
            text-align: justify !important;
        }
    </style>
@endsection
