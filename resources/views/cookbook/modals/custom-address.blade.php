<!-- Scopped styles -->
<style>
    .modal-body {
        display: flex; 
        width: 100%; 
        /* height: 150px;  */
        flex-direction: column; 
        /* background-color: #1f943a;  */
        overflow: hidden;
    }

    .modal-subtitle {
        display: block;
        font-size: 14x;
        /* color: #ffffff; */
    }

    
    /* Responsive settings */
    @media screen and (max-width: 500px){
        iframe {
            width: 300px; !important
            /* height: 100%; */
        }
    }
</style>

<div class="modal fade" id="custom_address" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Set custom shipping address for this order</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>

    <div class="modal-body">
        <div id="set_shipping_addess_wrapper" class="container position-relative bg-white p-2 mb-2">
            <span class="d-block" style="color:#19c942; font-size:14px;">*If not set, your default address ({{ auth()->user()->pick_up_address }}) will be used.</span>
            <div class="d-flex flex-column justify-content-start align-items-center" style="max-width: 700px;">
                <div class="w-100 my-2">
                    <label for="pick_store" class="d-flex justify-content-start align-items-center">
                        <span class="mr-2">Pick up at the Hurlingham store</span>
                        <input type="checkbox" checked name="pick_store" id="pick_store" style="width: 30px; height: 30px; margin: 0 10px;"> 
                    </label>
                    <span style="font-size:14px; color:rgb(160, 160, 160);">(*click box to select/unselect pick up)</span>
                </div>

                or

                <div class="w-100 my-2">
                     <label for="pick_custom" class="w-100">
                        <span class="ml-2">Write a Delivery address</span>
                        <input type="text" class="form-control pick_custom" name="pick_custom" id="pick_custom" required></input>
                    </label>
                </div>  
            </div>
            <div>
                <button id="save_shipping_address" class="btn btn-success btn-lg">Save</button>
            </div>

            <div class="position-absolute d-none justify-content-center align-items-center w-100 h-5 bg-info" 
            style="top: 0; left: 0; height:100%; opacity:0.6;"
            id="saving_loader">
                <i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i>
                <span class="sr-only">Saving...</span>
            </div>

        </div>

        <div class="d-none w-100 h-5 bg-info mb-2 text-center p-4" 
        style="color:white;" 
        id="success_loader">
            Success, custom shipping address set <i class="fa fa-check-circle fa-3x"></i>
        </div>
    </div>
        
    </div>
    </div>
</div>

<script>
    const shippingAddressWrapper = document.getElementById('set_shipping_addess_wrapper');
    const saveShippingAddressBtn = document.getElementById('save_shipping_address');

    //loaders
    const savingLoader = document.getElementById('saving_loader');
    const successLoader = document.getElementById('success_loader');

    //fields
    const pickStoreCheckbox = document.getElementById('pick_store');
    const pickCustomInput = document.getElementById('pick_custom');

    //event listeners
    pickCustomInput.disabled =true; // disable input field on first load
    pickStoreCheckbox.addEventListener('change', function() {
        pickCustomInput.disabled = this.checked;

        if (this.checked) {
            pickCustomInput.value = '';
        }
    });

    saveShippingAddressBtn.addEventListener('click', function() {
        // console.log('custom: '+pickCustomInput.value + ' pick Hullingham store: '+pickStoreCheckbox.checked)

        const CustomValue = pickCustomInput.value
        const StoreCheckbox = pickStoreCheckbox.checked
        const condition = CustomValue || StoreCheckbox;

        if (!condition) {
            alert("Please check the store option or fill in a custom address before saving")
        }else{
            savingLoader.classList.replace("d-none", "d-flex");

            // Prepare custom address to send
            customAddress = StoreCheckbox ? 'Hurlingham Store' : CustomValue;

            fetch("/cookbook/cart/checkout/setCustomAddress", {
            method: "POST",
            body: JSON.stringify({
                "_token": "{{ csrf_token() }}",
                'custom_address': customAddress,
                'transReference': @json($transReference)
            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
            })
            .then(response => {
                if (!response.ok) {
                throw new Error('Network response failed');
                }
                
                if (response.ok){
                    savingLoader.classList.replace("d-flex", "d-none");
                    shippingAddressWrapper.classList.add("d-none");
                    successLoader.classList.replace("d-none", "d-block");
                }

                return response.json(); // we return the response.json() Promise
            })
            .then(data => {
                if (data.success) {
                    document.getElementById("address").innerHTML = data.address;
                }
                // console.log('Data received:'+ data.address + ' '+ data.success);
            })
            .catch(error => {
                console.error('There was a problem with the fetch operation:', error);
            });
            // .then((json) => console.log(json));
        }

    });
</script>