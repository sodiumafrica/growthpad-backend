@php
    $products = json_decode($item->payment->serialized_products);
@endphp

Delivery/Pick Up Station: {{ $item->payment->pick_up_station ?? auth()->user()->pick_up_address }}

<table class="table table-bordered">
    <thead>
        <tr>
        <th scope="col">Product Name</th>
        <th scope="col">Unit price</th>
        <th scope="col">Quantity/Download</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($products as $product )     
            <tr>
                <td>{{$product->name}}</td>
                <td>{{$product->price}}</td>
                <td>
                    @if(isset($product->type) && $product->type == 'recipe')
                        <a target="_blank" class="btn btn-outline-primary btn-sm pull-left"
                            href="/cookbook/download/{{ $item->id }}/{{$product->id}}">
                            <i class="fa fa-download fa-fw"></i> Download
                        </a>
                    @else
                        {{$product->quantity}}
                    @endif
                </td>
            </tr>
        @endforeach
    </tbody>
    </table>

    Grand Total: {{ $item->payment->amount }}