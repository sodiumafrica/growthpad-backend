<div class="modal fade" id="purchase" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Purchase Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>

    <div class="modal-body">
       
    </div>
        
    </div>
    </div>
</div>

<script>
 
  $(document).ready(function() {
    $('.view-btn').on('click', function() {
      var id = $(this).data('id');
      
      $('#purchase .modal-body').html('<div class="position-relative d-flex justify-content-center align-items-center w-100 h-5 bg-white" style="top: 0; left: 0; height:100%; opacity:0.6;" id="saving_loader"> <i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i> <span class="sr-only">Saving...</span> </div>');
      // Make an AJAX request to fetch the row-specific data
      $.ajax({
        url: '/cookbook/my-purchases/' + id, // Get specific purchase
        type: 'GET',
        success: function(response) {
          // Populate the modal with the data from the response
          $('#purchase .modal-body').html(response);
        },
        error: function(xhr) {
          // Handle error
          console.log(xhr.responseText);
        }
      });
    });
  });

</script>