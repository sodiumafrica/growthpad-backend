@extends('layout.recipe')

@section('content')

<!-- slider_area_start -->
<div class="slider_area">
    <div class="single_slider  d-flex align-items-center"
        style="background-image: url('{{ asset('recipe/img/purchase.jpg') }}')">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-xl-8 ">
                    <div class="slider_text text-center">
                        <div class="text">
                            <h3>
                                Here are your purchases
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- slider_area_end -->

<div class="recepie_videoes_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                @if(request()->status == 'PESAPAL')
                <div class="alert alert-info">
                    Payment Processing! Your cookbook will be available to you in this page once the transaction has
                    been successfully processed. Please give upto 3 minutes
                </div>
                @endif
                @if ($purchases->count())
                {{-- @dd($purchases) --}}
                <table class="table table-bordered">
                    <tr>
                        <th>Purchase Amount</th>
                        <th>Transaction Reference</th>
                        <th>Processed On</th>
                        <th colspan=2>Status</th>
                    </tr>
                    @foreach ($purchases as $item)
                    <tr>
                        {{-- @dd($item->payment->amount) --}}
                        <td>{{ $item['payment']['amount'] }}</td>
                        {{-- @if ($item->product_type == 'vegetable')
                            <td>{{ config('cookbook.veges')[$item->product_key]['name'].'/'.config('cookbook.veges')[$item->product_key]['engName'] }}</td>
                        @else
                            <td>Ksh {{ config('cookbook.products')[$item->product_key]['price'][app()->getLocale('en')] }}</td>
                        @endif --}}
                        <td>{{ $item['payment']['processor'] }} ({{ $item['payment']['transaction_reference'] }})</td>
                        <td>{{ $item['created_at'] }}</td>
                        <td>
                            {{ $item['payment']['pesapal_status'] }}

                            {{-- <a target="_blank" class="btn btn-outline-primary btn-sm pull-right"
                                href="/cookbook/download/{{ $item->id }}">
                                <i class="fa fa-download fa-fw"></i> Download
                            </a> --}}
                            <a href="#!"  data-toggle="modal" data-target="#purchase"
                                data-id="{{ $item['id'] }}"
                                {{-- data-array="{{ $products }}" --}}
                                class="btn btn-outline-primary btn-sm pull-right view-btn">
                                <i class="fa fa-file-text-o fa-fw"></i> View
                            </a>
                        </td>
                    </tr>
                 @endforeach
                </table>
                @else
                <div class="alert alert-danger">
                    You have not purchased anything yet
                </div>
                @endif

                {{-- View purchase modal --}}
                @include('cookbook.modals.view-purchase')
            </div>
        </div>
    </div>
</div>

@endsection
