<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Complete Order</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
            <h6 style="display: block; width: 100%; font-size: 15px;">(All prices exclude shipment costs)</h6>
            <form method="post" action="/order/veges">
                @csrf
                <div class="form-group">
                    <label>Full Names</label>
                    <input type="text" class="form-control" name="names" required>
                </div>
                <div class="form-group">
                    <label>Total Price</label>
                    <input type="text" class="form-control total" name="total" readonly required>
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="text" class="form-control" name="email" required>
                </div>
                <div class="form-group">
                    <label>Telephone</label>
                    <input type="text" class="form-control" name="telephone" required>
                </div>
                <div class="form-group">
                    <input type="checkbox" name="selfpick" id="selfpick">
                    <label for="selfpick">Self-Picking at the Hurlingham store</label>
                    <br>
                    Or
                    <br>
                    <label>Enter location information</label>
                    <textarea type="text" class="form-control" name="location" id="location" required></textarea>
                </div>
                    <input type="hidden" name="order-items" id="orders">
                    <button class="btn btn-primary" type="submit">Place Order</button>
            </form>
        </div>
        
    </div>
    </div>
</div>

<script>
    $(function(){
        $('#exampleModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var item = button.data('item') // Extract info from data-* attributes
            var arrayItems = button.data('array') // Extract info from data-* attributes
            var modal = $(this)
            modal.find('#orders').val(JSON.stringify(arrayItems))
            modal.find('.total').val(item)
        })

        $('#selfpick').change(function(e) {
            var checkboxVal = $(this).is(":checked");
            if (checkboxVal) {
                $('#location').attr('readonly', 'readonly');
                $('#location').val('N/A');
            }else{
                $('#location').removeAttr('readonly');
                $('#location').val('');
            }
        })
    })
</script>