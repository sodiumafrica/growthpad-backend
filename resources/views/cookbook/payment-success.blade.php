@extends('layout.recipe')

@section('content')

    <div class="recepie_videoes_area mt-4">
        <div class="container">
            <div class="row alert alert-success" role="alert">
                <h3 class="text-center">
                    Checkout complete!
                </h3>
            </div>
            <div class="row">
                <div class="col ml-auto d-flex align-items-center justify-content-center">
                    <div class="text-left text-dark">
                        @if ($payment->pick_up_address == 'Hurlingham Store' || (is_null($payment->pick_up_address) && auth()->user()->pick_up_address == 'Hurlingham Store'))
                            Pick up your order(s) at the Hurlingham office within 2 days and bring the transaction message with you. 
                            Thank you for shopping with us!!.
                        @else
                            Support team from IREN Growthpad will get in touch soon and arrange for the delivery of your order(s) to {{ $payment->pick_up_address ?? auth()->user()->pick_up_address }}. 
                            Thank you for shopping with us!!.
                        @endif
                        <br>
                        <br>
                        For any questions or feedback, please feel free to contact us via <a href="mailto:growthpad@irenkenya.com">growthpad@irenkenya.com</a>. We value your continued support.
                    </div>
                </div>
                
                <div class="col text-center text-success">
                    <i class="fa fa-check-circle fa-4x" style="font-size: 15vh"></i>
                    <br>
                    <br>
                    <h1>Your payment has been successfully processed!</h1>
                    <br>
                    <br>
                    <a href="/cookbook/my-purchases" class="btn btn-success">View my purchases</a>
                </div>

                <br>
                <br>
                <br>
                <br>
                <br>
            </div>
        </div>
    </div>

@endsection
