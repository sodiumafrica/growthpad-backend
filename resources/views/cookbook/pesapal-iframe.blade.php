<!-- Scopped styles -->
<style>
    .modal-body {
        display: flex; 
        width: 100%; 
        height: 550px; 
        flex-direction: column; 
        /* background-color: #1f943a;  */
        overflow: hidden;
    }

    .modal-subtitle {
        display: block;
        font-size: 14x;
        /* color: #ffffff; */
    }

    
    /* Responsive settings */
    @media screen and (max-width: 500px){
        iframe {
            width: 300px; !important
            /* height: 100%; */
        }
    }
</style>

<div class="modal fade" id="pesapal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Complete Payment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>

    <div class="modal-body">
        <h6 class="modal-subtitle" id="exampleModalLabel">*Please wait till payment is processed after paying, you will be redirected.</h6>
        <iframe src="{{$paymentUrl}}" frameborder="0" height="500"></iframe>
        {{-- {{$paymentUrl}} --}}
    </div>
        
    </div>
    </div>
</div>

<script>
  //
</script>