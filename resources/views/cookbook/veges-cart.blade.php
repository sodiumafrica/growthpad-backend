@extends('layout.recipe')

@section('content')

    <!-- slider_area_start -->
    {{-- <div class="slider_area">
        <div class="single_slider  d-flex align-items-center"
             style="background-image: url('{{ asset('recipe/img/purchase.jpg') }}'); max-width: 100%; height: auto;">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-8">
                        <div class="slider_text text-center">
                            <div class="text">
                                <h3>
                                    Your Vegetables shopping cart
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}

    <div class="slider_area container position-relative">
        <div class="single_slider  d-flex justify-content-center col-12"
             style="margin-top:75px; max-height: 300px; width:100%;">
             <img src="/recipe/img/purchase.jpg" style="object-fit: cover; width: inherit; height: 300px; border-bottom-left-radius:50px; border-bottom-right-radius:50px;">
        </div>
        <h3 class="position-absolute" style="top: 40%; left:20%; font-size: 40px; font-weight:600; color:rgb(183, 255, 183)">Your Vegetables Cart</h3>
    </div>
    <!-- slider_area_end -->

    <div class="recepie_videoes_area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <a name="pesapal"></a>
                    @if ($items->count())
                        <table class="table table-bordered">
                            <tr>
                                <th>Vegetable Name</th>
                                <th>Price</th>
                                <th>Quantity</th>
                            </tr>
                            @php
                                $totalKES = 0;
                                // $totalEUR = 0;
                            @endphp
                            @foreach ($items as $item)
                                @php
                                    $raw = $item->rawId();
                                    $vege = config('cookbook.veges')[$item->id];
                                    $totalKES = $totalKES + $item['total'];
                                    // dd($item['qty']);
                                    // $totalEUR = $totalEUR + $item['price']['de'];
                                @endphp
                                <tr>
                                    <td>{{ $item['qty'] }}</td>
                                    <td>
                                        {{ (app()->getLocale('en') == 'de') ? 'EUR' : 'KES' }}
                                        {{ $item['price'] }}
                                    </td>
                                    <td>
                                        {{ $item['qty'] }}
                                        <span class="float-right">
                                            <a href="/cookbook/cart/remove/vegetable/{{ $raw }}" class="text-danger">Remove Item</a>
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                            <tr>
                                <th>Grand Total</th>
                                <th>
                                    {{ (app()->getLocale('en') == 'de') ? 'EUR' : 'KES' }}
                                    {{  $totalKES }}
                                </th>
                                <th></th>
                            </tr>
                        </table>
                        <a href="/cookbook/cart/remove/vegetable/all" class="text-danger">Remove everything</a>
                        <br>
                        <br>

                        @if (!auth()->guard()->check())
                            <a href="#!"  data-toggle="modal" data-target="#exampleModal"
                                data-item="{{ @$totalKES }}"
                                data-array="{{ $items }}"
                                class="btn btn-primary btn-lg mt-3 mb-5">
                                Place Order
                            </a>
                        @endif
                        
                        @auth
                            @if (!request()->has('checkout'))
                                <div>
                                    <input type="checkbox" style="transform: scale(2); padding: 10px;" id="check_location" checked>
                                    <label for="check_location" style="font-size: 20px; margin-left:5px;">Are you in Kenya?</label>
                                </div>
                            @endif
                        @endauth

                        <!-- Order vegetables form -->
                        @include('cookbook.order-veges')

                        
                        {{-- @dump(session()->all()) --}}
                    @auth
                        <div class="col-12 text-center">
                            @if(app()->getLocale() == 'en')
                                @if(!request()->has('checkout'))
                                                                                       
                                    <a href="/cookbook/cart/display?checkout=mpesa&type=vegetable&total={{$totalKES}}"
                                       id="mpesa_btn"
                                       class="btn btn-success btn-lg mt-3 mb-5">
                                        <i class="fa fa-credit-card fa-fw"></i> Lipa na M-PESA
                                    </a>
                                    <a href="#!" id="place_order_trigger"  data-toggle="modal" data-target="#exampleModal"
                                        data-item="{{ @$totalKES }}"
                                        data-array="{{ $items }}"
                                        class="btn btn-primary btn-lg mt-3 mb-5 d-none">
                                                    Place Order
                                    </a>
                                    {{-- <a href="/cookbook/cart/display?checkout=paypal"
                                       class="btn btn-success btn-lg mt-3 mb-5">
                                        <i class="fa fa-credit-card fa-fw"></i> Checkout with Paypal
                                    </a> --}}
                                @else
                                    @if(request()->checkout == 'mpesa')
                                        <div class="row">
                                            <div class="col">
                                                <div class="card card-body">
                                                    <h4>Step 1: Lipa na M-PESA</h4>
                                                    <ul class="list-group list-group-flush">
                                                        <li class="list-group-item">
                                                            1. Open the <b>M-PESA</b> menu in your phone
                                                        </li>
                                                        <li class="list-group-item">
                                                            2. Navigate to <b>Lipa Na M-PESA</b>
                                                        </li>
                                                        <li class="list-group-item">
                                                            3. Select <b>Buy Goods and Services</b>
                                                        </li>
                                                        <li class="list-group-item">
                                                            4. Enter till number <b>5164955</b>
                                                        </li>
                                                        <li class="list-group-item">
                                                            5. Enter amount <b>{{ $totalKES }}</b>
                                                        </li>
                                                        <li class="list-group-item">
                                                            6. Enter your PIN and confirm
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="card card-body text-center">
                                                    <h4>Step 2: Confirm your payment</h4>
                                                    <hr>

                                                    <div class="alert alert-info frmChecking">
                                                        We are checking for a payment from the number you used to
                                                        register ({{ auth()->user()->telephone }})
                                                    </div>
                                                    <div class="text-center frmLoader">
                                                        <img src="{{ asset('images/loading.gif') }}" width="300px">
                                                    </div>

                                                    <div class="alert alert-danger frmUnpaid">
                                                        <h5>Payment not found!</h5>
                                                        We have not received a payment
                                                        from {{ auth()->user()->telephone }}. Checking again in a bit...
                                                    </div>

                                                    <div class="alert alert-success frmPaid">
                                                        <h5>Payment Received!</h5>
                                                        We have received your payment via MPESA.
                                                        <a href="#!" class="btn btn-success btnComplete">Complete
                                                            Checkout</a>
                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                    {{-- @elseif(request()->checkout == 'pesapal')
                                        {!! $iframe !!}
                                    @elseif(request()->checkout == 'paypal')
                                        <span id="paypal-button-container"></span> --}}
                                    @endif
                                @endif
                            @else
                                <span id="paypal-button-container"></span>
                            @endif
                        </div>
                    @endauth

                    @else
                        <div class="alert alert-danger">
                            You have not added anything to the cart
                        </div>

                        <a href="/cookbook#shop" class="btn btn-success btn-lg btn-block">
                            Shop items here
                        </a>
                    @endif


                </div>
            </div>
        </div>
    </div>



@endsection

@push('footer-scripts')
@auth
    <script>
        $('#check_location').change(function() {
            if ($(this).is(':checked')) {
                $('#mpesa_btn').removeClass('d-none')
                $('#place_order_trigger').addClass('d-none')
            } else {
                $('#place_order_trigger').removeClass('d-none')
                $('#mpesa_btn').addClass('d-none')
            }
        })

        var busy = false
        var payment;
        var token;

        $(function () {
            // paypal.Buttons({
            //     createOrder: function (data, actions) {
            //         return actions.order.create({
            //             purchase_units: [{
            //                 amount: {
            //                     currency_code: "EUR",
            //                     value: "{{ @$totalEUR }}"
            //                 }
            //             }]
            //         });
            //     },
            //     onApprove: function (data, actions) {
            //         return actions.order.capture().then(function (details) {
            //             alert('Transaction successfully completed. Please click OK and wait while we process...')
            //             location.href = '/cookbook/paypal/success?payload=' + JSON.stringify(details) + '&total={{ @$totalEUR }}'
            //         });
            //     }
            // }).render('#paypal-button-container')

            // Detect payment
            $('.frmUnpaid, .frmPaid').hide()
            checkPayment()

            setInterval(function () {
                checkPayment()
            }, 10000)

            // Update busy
            setInterval(function () {
                // console.log(busy)
                if (busy) {
                    $('.frmLoader, .frmChecking').show()
                    $('.frmUnpaid, .frmPaid').hide()
                } else {
                    $('.frmLoader, .frmChecking').hide()
                }
            }, 500)

            $('.btnComplete').click(function () {
                window.location.href = '/cookbook/cart/mpesa/finish/' + token
            })

        })

        function checkPayment() {
            if (!busy) {
                busy = true
                fetch('{{ url('api/payment/detect?msisdn='. auth()->user()->telephone) }}')
                    .then(function (response) {
                        busy = false

                        if (response.status !== 200) {
                            console.log('Status ' + response.status + ' => An error occured while requesting for payment data')
                            return
                        }

                        response.json().then(function (res) {
                            // console.log(res)

                            if (res.status == 'FAILED') {
                                $('.frmUnpaid').show()
                                $('.frmPaid').hide()
                            } else {
                                $('.frmPaid').show()
                                $('.frmUnpaid').hide()
                                token = res.token
                            }
                        })
                    })
            }
        }

    </script>
@endauth

@endpush
