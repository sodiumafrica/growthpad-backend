@extends('layout.recipe')

<style>
    .recepie_info label{
        font-size: 20px;
    }

    @media only screen and (max-width: 768px) {
        .recepie_info label{
        font-size: 14px;
    }
    }
</style>

@section('content')

    <!-- slider_area_start -->
    <div class="slider_area container">
        <div class="single_slider  d-flex justify-content-center col-12"
             style="margin-top:55px; height: auto; width:100%; background-color: black;">
             <img src="{{ $product['product_bag'] }}" style="max-width: 100%; height: auto;">
        </div>
    </div>
    <!-- slider_area_end -->

    <div class="recepie_videoes_area">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6 text-center">
                    <div class="recepie_info">
                        {{-- <img src="{{ asset($product['picture']) }}" class="img-thumbnail mb-5"> --}}
                        <div class="list-group text-left">
                            <div class="list-group-item list-group-item-action flex-column align-items-start">
                                <small>Product Name</small>
                                <p class="mb-1">{{ $product['name'].' / '.$product['engName'] }}</p>
                            </div>
                            <div class="list-group-item list-group-item-action flex-column align-items-start">
                                <small>Servings</small>
                                <p class="mb-1">60g, 250g, 500g</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-6 text-left">
                    <div class="recepie_info">
                        <form id="addtocart" action="{{ route('addVegiesOrderToCart', ['id' => $key]) }}" method="POST">
                            @csrf

                            <label for="quantity" class="form-label">Quantity</label>
                            <label for="veges_select" class="form-label ml-5">Choose your preferred serving</label>
    
                            <div class="input-group">
                                <input type="number" min="1" value="1" name="quantity" id="quantity" class="col-3">
                                
                                <select class="form-select col" name="veges_select" id="veges_select" aria-label="Default select example">
                                    @foreach ( @$product['price'] as $size => $price)
                                        <option value="{{ @$size.'-'.@$price['retail'] }}">{{ @$size.' @ KES '.@$price['retail'].' per packet.' }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <input type="hidden" name="serving" id="serving" value="">

    
                            <div class="input-group mt-2 mb-4">
                                <span class="input-group-text" id="totalLabel">Total</span>
                                <input type="text" class="form-control" name="total" id="total" readonly aria-label="Total" aria-describedby="totalLabel">
                            </div>
    
                            <button class="btn btn-primary btn-lg">
                                <i class="fa fa-shopping-cart fa-fw"></i> Add to cart
                            </button>
                        </form>
                    </div>
                </div>


            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal" id="exampleModal" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 10000000;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Confirm Payment</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label>Enter your MPESA payment code</label>
                        <input type="text" class="form-control txtCode">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btnCheck">Confirm Payment</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function () {

         

            function calcTotal() {
                    let vege = $('#veges_select').val().split('-')
                    let price = vege[1]
                    let quantity = $('#quantity').val()
                    let total = price * quantity

                    //set serving value
                    $('#serving').val(vege[0])
                    //set total value
                    $('#total').val(total)
                }

                calcTotal();
                $('#veges_select').change(calcTotal);
                $('#quantity').change(calcTotal);
        })
    </script>

@endsection
