@component('mail::message')

You have an new vegetable order.

@component('mail::panel')
Customer Names: {{ $order['names'] }} <br>
Email: {{ $order['email'] }} <br>
Telephone: {{ $order['telephone'] }} <br>
@if (isset($order['selfpick']))
Delivey: Picking at the Hurlingham store <br>
@else
Delivey: {{ $order['location'] }} <br>
@endif

@component('mail::table')
| Product       | Quantity         | Serving  | Total |
| :------------- |:-------------:| :--------:| :--------:|
@foreach($order_items as $item)
| {{ $item['name'] }} | {{ $item['qty'] }} | {{ $item['serving'] }} | {{ $item['total'] }} |
@endforeach
@endcomponent

Grand Total: {{ $order['total'] }} <br>
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent