@component('mail::message')

You have an new order.

@component('mail::panel')
Customer Names: {{ $order['names'] }} <br>
Email: {{ $order['email'] }} <br>
Telephone: {{ $order['telephone'] }} <br>
Delivey/Pick-Up: {{ $order['location'] }} <br>

Transaction Reference: {{ $order['tnxRef'] }} <br>

@component('mail::table')
| Product       | Quantity         | Price  |
| :------------- |:-------------:| :--------:|
@foreach($order_items as $item)
| {{ $item['name'] }} | {{ $item['quantity'] }} | {{ $item['price'] }} |
@endforeach
@endcomponent

Grand Total: {{ $order['total'] }} <br>
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent