@component('mail::message')
# Hello {{ $name }},

Your vegetables order has been sent successfully and is being processed.
<br>
Someone will be in touch soon!

Thanks,<br>
{{ config('app.name') }}
@endcomponent
